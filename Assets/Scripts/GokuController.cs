using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GokuController : MonoBehaviour
{
    private SpriteRenderer sr;
    private Animator animator;
    private Rigidbody2D rb2d;

    public float speed = 30;
    
    public const int Run = 0;
    public const int VolarNube = 1;

    public float up = 2;
    public float down = 5;
    

    private const string VuelaGoku = "ObjetoVolar";
    
    private const string NoVuela = "DejaVolar";

    // Start is called before the first frame update
    void Start()
    {
        
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
            sr.flipX = false;
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb2d.velocity = new Vector2(-speed, rb2d.velocity.y);
            sr.flipX = true;
        }
        else
        {
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x,up);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x,-up);
        }

    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag(VuelaGoku) )
        {
            Destroy(other.gameObject);
            rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
            CambiarAnimacion(VolarNube);
            rb2d.gravityScale = 0;
        }
        else if (other.gameObject.CompareTag(NoVuela))
        {
            Destroy(other.gameObject);
            rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
            CambiarAnimacion(Run);
            rb2d.gravityScale = 1;
        }
    }

    private void CambiarAnimacion(int animacion)
    {
        animator.SetInteger("Estado", animacion);
    }
    
    
}
